package com.andymur.playground.dropwizard.resources;

import com.andymur.playground.dropwizard.model.Planet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by andymur on 2/18/15.
 */
@Component
@Path("/playground")
@Produces(MediaType.APPLICATION_JSON)
public class PlaygroundResource {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final String CHECK = "Hi";

    @Autowired
    @Qualifier(value = "pluto")
    Planet planet;


    @GET
    public String check() {
        logger.debug("check.enter = {}", planet);
        return planet.getName();
        //return planet;
    }
}
