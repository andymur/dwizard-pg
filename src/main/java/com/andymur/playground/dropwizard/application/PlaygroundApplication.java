package com.andymur.playground.dropwizard.application;

import com.github.nhuray.dropwizard.spring.SpringBundle;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import com.andymur.playground.dropwizard.resources.PlaygroundResource;
import org.springframework.beans.BeansException;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by andymur on 2/18/15.
 */
public class PlaygroundApplication extends Application<PlaygroundConfiguration> {

    public static final String BASE_PACKAGE = "com.andymur";

    public static void main(String[] args) throws Exception {
        new PlaygroundApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<PlaygroundConfiguration> playgroundConfigurationBootstrap) {
        playgroundConfigurationBootstrap.addBundle(
                new SpringBundle<PlaygroundConfiguration>(applicationContext(),
                        true, true, true)
        );
    }

    @Override
    public void run(PlaygroundConfiguration playgroundConfiguration, Environment environment) throws Exception {
        //environment.jersey().register(PlaygroundResource.class);
    }

    private ConfigurableApplicationContext applicationContext() throws BeansException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan(BASE_PACKAGE);
        return context;
    }
}
