package com.andymur.playground.dropwizard.application;

import com.andymur.playground.dropwizard.model.Planet;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by andymur on 3/8/15.
 */
@Configuration
@ComponentScan(basePackages = "com.andymur")
public class ContainerConfiguration {

    @Bean(name = "pluto")
    @Qualifier(value = "pluto")
    public Planet getPluto() {
        return new Planet(4.0, "Pluto");
    }
}
