package com.andymur.playground.dropwizard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Objects;
import org.springframework.stereotype.Component;

/**
 * Created by andymur on 2/21/15.
 */
@Component
public class Planet {
    private double mass;
    private String name;

    public Planet() {
        mass = 3.0;
        name = "Earth";
    }

    public Planet(double mass, String name) {
        this.mass = mass;
        this.name = name;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this).add("mass", mass).add("name", name).toString();
    }

    public double getMass() {
        return mass;
    }

    //@JsonProperty
    public String getName() {
        return name;
    }
}
